" plugin setup
set nocompatible

" vundle
filetype off
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'VundleVim/Vundle.vim'
Plugin 'tpope/vim-fugitive'
Plugin 'scrooloose/nerdcommenter'
Plugin 'scrooloose/nerdtree'
Plugin 'vim-syntastic/syntastic'
Plugin 'sjl/gundo.vim'
Plugin 'elzr/vim-json'
Plugin 'plasticboy/vim-markdown'
Plugin 'fatih/vim-go'
" breaks on startup due to maktaba error:
"   Error detected while processing function
"   maktaba#flags#AddCallback[1]..maktaba#ensure#IsCallable:
"   line    9:
"Plugin 'google/vim-maktaba'
"Plugin 'google/vim-codefmt'
"Plugin 'google/vim-glaive'
call vundle#end()
filetype plugin indent on

" ggl stuff
if filereadable(expand("~/.vimrc.ggl"))
  source ~/.vimrc.ggl
endif

" editing stuff
set tabstop=2
set shiftwidth=2
set expandtab
set ruler
set nu
set backspace=2
set wrap
set textwidth=80
set spell spelllang=en_us
set matchpairs+=<:>

" syntax stuff
syntax on
filetype on
set fileformats=unix

" colors
set background=light
highlight Normal ctermfg=NONE ctermbg=NONE
set colorcolumn=+1
highlight ExtraWhitespace ctermbg=red guibg=red
match ExtraWhitespace /\s\+\%#\@<!$/

" plugin mappings

" codefmt
map <C-f> :FormatCode<CR>
" gundo
map <leader>g :GundoToggle<CR>
" nerdtree
map <leader>n :NERDTreeToggle<CR>
let NERDTreeWinSize = 40
" nerdcomment
map cc <leader>cc
map cu <leader>cu

" plugin customization

" vim-fugative
set statusline=%{fugitive#statusline()}\ %F%=%l/%L:%c

" key mappings
imap jj <Esc><Esc>
imap kk <Esc><Esc>
vnoremap j <Down>
vnoremap k <Up>
map <C-J> <C-D>zz
map <C-K> <C-U>zz

" disable autocomplete of included files
set complete-=i

" must be last line in config
filetype plugin indent on
