au BufNewFile,BufRead *.md set filetype=mkd
set wrap
set tw=80
set nofoldenable
