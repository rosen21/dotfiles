// uses Mac OS X Screen ID which can be set using the display preferences
slate.config("orderScreensLeftToRight", false);
var mainMonitorScreenRef = "0";
var minorMonitorScreenRef = "1";

var smallCenterOp = slate.operation("move", {
  "x" : "screenSizeX*5/16",
  "y" : "screenOriginY",
  "width" : "screenSizeX*3/8",
  "height" : "screenSizeY",
  "screen" : mainMonitorScreenRef
});

var largeCenterOp = slate.operation("move", {
  "x" : "screenSizeX*2/11",
  "y" : "screenOriginY",
  "width" : "screenSizeX*7/11",
  "height" : "screenSizeY",
  "screen" : mainMonitorScreenRef
});

var fullScreenOp = slate.operation("move", {
  "x" : "screenOriginX",
  "y" : "screenOriginY",
  "width" : "screenSizeX",
  "height" : "screenSizeY",
  "screen" : mainMonitorScreenRef
});

var minorFullScreenOp = slate.operation("move", {
  "x" : "screenOriginX",
  "y" : "screenOriginY",
  "width" : "screenSizeX",
  "height" : "screenSizeY",
  "screen" : minorMonitorScreenRef
});

var leftHalfOp = function(win) {
  slate.operation("move", {
    "x" : win.screen().rect().x,
    "y" : win.topLeft().y,
    "width" : win.screen().rect().width / 2,
    "height" : win.size().height,
    "screen" : win.screen().id(),
  }).run();
};

var rightHalfOp = function(win) {
  slate.operation("move", {
    "x" : win.screen().rect().width / 2,
    "y" : win.topLeft().y,
    "width" : win.screen().rect().width / 2,
    "height" : win.size().height,
    "screen" : win.screen().id(),
  }).run();
};

var topHalfOp = function(win) {
  slate.operation("move", {
    "x" : win.topLeft().x,
    "y" : win.screen().rect().y,
    "width" : win.size().width,
    "height" : win.screen().rect().height / 2,
    "screen" : win.screen().id(),
  }).run();
};

var bottomHalfOp = function(win) {
  slate.operation("move", {
    "x" : win.topLeft().x,
    "y" : win.screen().rect().height / 2,
    "width" : win.size().width,
    "height" : win.screen().rect().height / 2,
    "screen" : win.screen().id(),
  }).run();
};

allApps = [
  "Terminal",
  "Google Chrome",
  "Preview",
  "TextEdit"
];


// ################################## Filters ##################################
var hangoutsFilter = function(windowObject) {
  var app = windowObject.app().name();
  var title = windowObject.title();
  var width = windowObject.size().width;
  return ((app == "Google Chrome")
      && (title !== undefined)
      && (title.match(/Hangouts/) || (width == 280)));
};

var secureShellFilter = function(windowObject) {
  var app = windowObject.app().name();
  var title = windowObject.title();
  return ((app == "Google Chrome")
          && (title !== undefined) 
          // TODO: fix so that this matches when not logged in
          && title.match(/.*anvil.*/));
};

var terminalFilter = function(windowObject) {
  var app = windowObject.app().name();
  var title = windowObject.title();
  return (app == "Terminal");
};

var mainChromeFilter = function(windowObject) {
  var app = windowObject.app().name();
  return (app == "Google Chrome") && window.isMain();
};

// ################################# Utilities #################################
var buildParams = function(defaultOp, exceptions) {
  return {
    "repeat" : true,
    "operations" : [
      function(windowObject) {
        exceptionFired = false;
        for (var i = 0; i < exceptions.length; i++) {
          exception = exceptions[i];
          if (!exceptionFired && exception["filter"](windowObject)) {
            windowObject.doOperation(exception["op"]);
            exceptionFired = true;
          }
        }
        if (!exceptionFired) {
          windowObject.doOperation(defaultOp);
        }
      }
    ]
  };
};

var buildLayoutDescription = function(defaultOp, exceptions) {
  if (exceptions == undefined) {
    exceptions = []
  }
  exceptions.push(
    {
      "name" : "hangouts",
      "filter" : hangoutsFilter,
      "op" : function(windowObject) {}
    }
  );
  desc = {};
  for (var i = 0; i < allApps.length; i++) {
    app = allApps[i];
    desc[app] = buildParams(defaultOp, exceptions);
  }
  return desc;
};

var allWindows = function(cb) {
  slate.eachApp(function(app) {
    app.eachWindow(function(win) {
      cb(win)
    });
  });
};

// ################################## Layouts ################################## 
var smallCenterLayout = slate.layout("smallCenter", 
                                    buildLayoutDescription(smallCenterOp));
slate.bind("7:cmd,shift",
           slate.operation("layout", { "name" : smallCenterLayout }), true);

var largeCenterLayout = slate.layout("largeCenter", 
                                    buildLayoutDescription(largeCenterOp));
slate.bind("8:cmd,shift",
           slate.operation("layout", { "name" : largeCenterLayout }), true);

var fullScreenLayout = slate.layout("fullScreen", 
                                    buildLayoutDescription(fullScreenOp));
slate.bind("0:cmd,shift",
           slate.operation("layout", { "name" : fullScreenLayout }), true);

var splitExceptions = [
  {
      "name" : "secureShell",
      "filter" : secureShellFilter,
      "op" : rightHalfOp
  },
  {
      "name" : "terminal",
      "filter" : terminalFilter,
      "op" : rightHalfOp
  },
]

var splitScreenLayout = slate.layout("splitScreen", 
                                      buildLayoutDescription(leftHalfOp,
                                        splitExceptions)
                                    );
slate.bind("9:cmd,shift",
           slate.operation("layout", { "name" : splitScreenLayout }), true);

// commands for sending the current screen left/right/up/down using vim keys
slate.bind("l:cmd,shift", rightHalfOp);
slate.bind("h:cmd,shift", leftHalfOp);
slate.bind("j:cmd,shift", minorFullScreenOp);
slate.bind("k:cmd,shift", fullScreenOp);
slate.bind("p:cmd,shift", topHalfOp);
slate.bind("n:cmd,shift", bottomHalfOp);

// ############################## Default Layouts ##############################
slate.default(["2560x1600", "1440x900"], largeCenterLayout);
slate.default(["1080x1920", "1440x900"], largeCenterLayout);
slate.default(["1440x900"], fullScreenLayout);

// ############################## Other Bindings ###############################
slate.embrToggle = false;
slate.bind("0:cmd,shift", function() {
  allWindows(function(win) {
    slate.log("slate_prefix: win.title()" + win.title());
    if (secureShellFilter(win) && (slate.embrToggle == false)) {
      slate.log("found secureShell");
      //var success = win.focus();
      //slate.log("slate_prefix: success: " + success);
      //slate.embrToggle = true;
    } else if (mainChromeFilter(win) && (slate.embrToggle == true)) {
      slate.log("found mainChrome");
      //var success = win.focus();
      //slate.log("slate_prefix: success: " + success);
      //slate.embrToggle = false;
    }
  });
});

slate.bind("1:cmd,shift", function() {
  allWindows(function(win) {
    slate.log("slate_prefix: win.title(): " + win.title());
    slate.log("slate_prefix: win.app().name(): " + win.app().name());
  });
});

