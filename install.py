#!/usr/bin/python
import os, sys, pprint, itertools, subprocess, time

rel_paths = [
             '.bash_profile',
             '.bashrc',
             '.dircolors',
             '.emacs',
             '.gitconfig',
             '.inputrc',
             '.ipython/profile_default/ipython_config.py',
             '.ipython/profile_default/startup/logger.py',
             '.screenrc',
             '.slate.js',
             '.vim',
             '.vimrc',
             ]
backup = os.path.expanduser('~/.dotfiles.%d.bak' % int(time.time()))

home = os.path.expanduser('~')
cwd = os.getcwd()

# do backup
cmd = ['mkdir', backup]
print ' '.join(cmd)
subprocess.call(cmd)
for rel_path in rel_paths:
    src = os.path.join(home, rel_path)
    dest = os.path.join(backup, rel_path)
    dest_dir, dest_f = os.path.split(dest)
    if not os.path.exists(dest_dir):
        print 'mkdir -p %s' % dest_dir
        os.makedirs(dest_dir)
    cp_cmd = ['cp', '-r', src, dest]
    print ' '.join(cp_cmd)
    subprocess.call(cp_cmd)

for rel_path in rel_paths:
    # remove old file and symlink new file
    src = os.path.join(cwd, rel_path)
    dest = os.path.join(home,rel_path)

    ln_cmd = ['ln', '-snf', src, dest]
    print ' '.join(ln_cmd)
    subprocess.call(ln_cmd)
