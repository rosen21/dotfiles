# google specific stuff
GGL_BASH_PROFILE=$HOME/.bashrc.ggl
if [ -a $GGL_BASH_PROFILE ]; then
  source $GGL_BASH_PROFILE
fi

# mac os x specific stuff
if [ ${HOME:0:5} = "/User" ]; then
    PATH="/Users/embr/Library/Python/2.7/bin:$PATH"

    # bash completion / brew stuff
    #if [ -f `brew --prefix`/etc/bash_completion ]; then
        #. `brew --prefix`/etc/bash_completion
    #fi

    export CLICOLOR=1

    export PIP_DOWNLOAD_CACHE=$HOME/Library/Caches/pip-downloads

    # The next line updates PATH for the Google Cloud SDK.
    #source '/Users/embr/src/google-cloud-sdk/path.bash.inc'

    # The next line enables shell command completion for gcloud.
    #source '/Users/embr/src/google-cloud-sdk/completion.bash.inc'
fi

export PATH="$PATH:$HOME/.cabal/bin"

export EDITOR=vim

#PS1="\[\033[\e[1;32m\]\u@\h:\W$\[\033[0;39m\] "

export TERM=xterm-256color
force_color_prompt=yes

alias vimd="vim -c \":NERDTreeToggle\""
alias ls='ls $LS_OPTIONS'
alias ll='ls -lh'
alias la='ls -la'
alias lt='ls -lt'
alias lrt='ls -lrt'
alias pa='prodaccess && source $HOME/.bashrc'
alias venv='source ~/.virtualenv/bin/activate'
alias imgcat='~/src/dotfiles/hterm-show-file.sh'

# homebrew python precedence
export PATH=/usr/local/bin:/usr/local/share/python:$HOME/.local/bin:$HOME/bin:$PATH
export PYTHONPATH="/usr/local/lib/python2.7/site-packages:$PYTHONPATH"
export IPYTHONDIR=~/.ipython

# MacTeX
export PATH=/usr/texbin:$PATH

# History stuff
# Some weird stuff to make multiple shells work together. The following setup
# attempts to ensure the following conditions:
#
# 1) each shell's default history (accessed by ctrl-r) only contains commands
#    exectuted in that shell.
# 2) executing ctrl-g will toggle between the local and global history file.
# 3) The global history file contains an up-to-date log of all commands in all
#    shells, potentially interleaved according to time of execution.

GLOBAL_HISTFILE=$HOME/.global_history

LOCAL_HISTFILE=$(mktemp)

export HISTFILE=$LOCAL_HISTFILE

LAST_CMD_HISTFILE=$(mktemp)

# Append last command to both local and global command history after each command
export PROMPT_COMMAND="
    history -w ${LAST_CMD_HISTFILE};\
    tail -n 1 ${LAST_CMD_HISTFILE} >> ${LOCAL_HISTFILE};\
    tail -n 1 ${LAST_CMD_HISTFILE} >> ${GLOBAL_HISTFILE};\
    history -c;\
    history -r ${HISTFILE};\
    ${PROMPT_COMMAND}"

# function to toggle between local and global shell history files.
function toggle_history_file {
  if [ "${HISTFILE}" = "${LOCAL_HISTFILE}" ]
  then
    HISTFILE=${GLOBAL_HISTFILE}
    echo "set HISTFILE to global hist file"
  else
    HISTFILE=${LOCAL_HISTFILE}
    echo "set HISTFILE to local hist file"
  fi
  history -cr ${HISTFILE}
  echo "clearing and reloading history"
}

# Bind ctrl-G to reverse global history search function
bind -x '"\C-g": "toggle_history_file"'

# Autocomplete screen sessions
complete -C "perl -e '@w=split(/ /,\$ENV{COMP_LINE},-1);\$w=pop(@w);for(qx(screen -ls)){print qq/\$1\n/ if (/^\s*\$w/&&/(\d+\.\w+)/||/\d+\.(\$w\w*)/)}'" screen

# Git branch in prompt
# copied from https://github.com/jimeh/git-aware-prompt/blob/master/prompt.sh
find_git_branch() {
  # Based on: http://stackoverflow.com/a/13003854/170413
  local branch
  if branch=$(git rev-parse --abbrev-ref HEAD 2> /dev/null); then
    if [[ "$branch" == "HEAD" ]]; then
      branch='detached*'
    fi
    git_branch="($branch)"
  else
    git_branch=""
  fi
}

find_git_dirty() {
  local status=$(git status --porcelain 2> /dev/null)
  if [[ "$status" != "" ]]; then
    git_dirty='*'
  else
    git_dirty=''
  fi
}

PROMPT_COMMAND="find_git_branch; find_git_dirty; $PROMPT_COMMAND"
PS1="\[\033[\e[1;32m\]\D{%FT%T} \u@\h\[\033[1;31m\]\$git_branch\[\033[1;35m\]\$git_dirty\[\033[0;39m\]$ "

# Python Matplotlib Plotting Workaround for virtualenvs
function frameworkpython {
    if [[ ! -z "$VIRTUAL_ENV" ]]; then
        #PYTHONHOME=$VIRTUAL_ENV /usr/local/bin/python "$@"
        PYTHONHOME=$VIRTUAL_ENV /usr/bin/python "$@"
    else
        #/usr/local/bin/python "$@"
        /usr/bin/python "$@"
    fi
}
