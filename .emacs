(setq-default indent-tabs-mode nil)
(setq-default tab-width 4)
(setq transient-mark-mode t)

(autoload 'js2-mode "js2" nil t)
(add-to-list 'auto-mode-alist '("\\.js$" . js2-mode))
(add-to-list 'auto-mode-alist '("\\.json$" . json-mode))