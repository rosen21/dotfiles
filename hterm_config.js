set_profile = function() {
  term_.prefs_.set('background-color', 'rgba(254, 247, 232, 1.0)');
  term_.prefs_.set('foreground-color', 'rgba(36, 155, 150, 1.0)');
  term_.prefs_.set('audible-bell-sound', '')
};
